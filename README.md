# Installation
## PI OS
Herunterladen von [Raspberry Pi OS Lite](https://www.raspberrypi.org/software/operating-systems/) und
installieren mit:
```shell
dd if=2021-05-07-raspios-buster-armhf-lite.img of=/dev/sda bs=4M conv=fsync status=progress
```
In der Partition namens **boot** muss eine Datei namens ssh erstellt werden.
Sonst hat man kein ssh zugriff auf denn Raspberry Pi.

## HomeMatic installieren
[Quelle](https://github.com/alexreinert/debmatic/blob/master/docs/setup/raspberrypi.md)

## HomeMatic Gerät neu anlernen
- Batterien entfernen
- Systemtaste drücken und gedrückt halten
- Batterien wieder einsetzen
- Warten bis der Systemtaste orange blickt, dann die Systemtaste einmal kurz loslassen und wieder drücken bis er grün leuchtet anschließend die Systemtaste wieder loslassen, das Thermostat startet nun neu
Anschließend in der CCU nach Geräte suchen, mit dem HomeMatic Gerät muss nun nicht mehr gemacht warten.
Es sollte nun automatisch gefunden werden.

# CC2531 Programmieren
[Quelle](https://www.smarthomejetzt.de/cc2531-ohne-debugger-mit-raspberry-pi-als-zigbee-usb-stick-flashen/)

Löst auch wenn iobroker denn Stick nicht mehr benutzen möchte das Problem.

```
PIN 39 Raspberry PI >> PIN 1 GND CC2531
PIN 38 Raspberry PI >> PIN 4 DD CC2531
PIN 36 Raspberry PI >> PIN 3 DC CC2531
PIN 35 Raspberry PI >> PIN 7 RST CC2531
```

```
./cc_chipid
    ID=b524
```

```
./cc_erase
./cc_write ../Z-Stack-firmware-master/coordinator/Z-Stack_Home_1.2/bin/default/CC2531ZNP-Prod.hex
```
