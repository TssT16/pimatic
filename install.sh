#!/bin/shell
#Quelle: https://github.com/alexreinert/debmatic/blob/master/docs/setup/raspberrypi.md

#--- debmatic ---
#sudo locale-gen en_US.UTF-8
export LANGUAGE=en_US.UTF-8
sudo apt update && sudo apt upgrade -y
wget -q -O - https://www.debmatic.de/debmatic/public.key | sudo apt-key add -
sudo bash -c 'echo "deb https://www.debmatic.de/debmatic stable main" > /etc/apt/sources.list.d/debmatic.list'
sudo apt update
sudo apt install build-essential bison flex libssl-dev -y
sudo apt install raspberrypi-kernel-headers pivccu-modules-dkms -y
sudo apt install pivccu-modules-raspberrypi -y

# Option 1: Bluetooth deaktivieren 
sudo bash -c 'cat << EOT >> /boot/config.txt
dtoverlay=pi3-disable-bt
EOT'
sudo systemctl disable hciuart.service

sudo sed -i /boot/cmdline.txt -e "s/console=serial0,[0-9]\+ //"
sudo sed -i /boot/cmdline.txt -e "s/console=ttyAMA0,[0-9]\+ //"
sudo reboot

echo "sudo apt install debmatic"


#--- ioBroker ---
#Quelle: https://www.modius-techblog.de/smart-home/iobroker-auf-dem-raspberry-pi-installieren-und-konfigurieren/
sudo useradd -m iobroker
curl -sL https://deb.nodesource.com/setup_12.x | sudo -E bash -
apt install -y nodejs
curl -sL https://iobroker.net/install.sh | bash -
